/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class SimpleActor extends Actor {

  /** @override */
  getRollData() {
    const data = super.getRollData();
    const shorthand = game.settings.get("worldbuilding", "macroShorthand");

    // Re-map all attributes onto the base roll data
    if ( !!shorthand ) {

      for ( let [k, v] of Object.entries(data.attributes) ) {
        if ( !(k in data) ) data[k] = v.value;
      }
      delete data.attributes;
    }

    // Map all items data using their slugified names
    data.items = this.data.items.reduce((obj, i) => {
      let key = i.name.slugify({strict: true});
      let itemData = duplicate(i.data);
      if ( !!shorthand ) {
        //for ( let [k, v] of Object.entries(itemData.attributes) ) {
        //  if ( !(k in itemData) ) itemData[k] = v.value;
        //}
        delete itemData["attributes"];
      }
      obj[key] = itemData;
      return obj;
    }, {});
    return data;
  }

  prepareData() {
      super.prepareData();

      const actorData = this.data;
      const data = actorData.data;
      const flags = actorData.flags;

      if(actorData.items !== undefined && actorData.items.length >0){
          //const weapons = actorData.items.find(i=>i.type === "weapon");
          var itemsByType = {};
          for (const item of actorData.items) {
              var list = itemsByType[item.type];
              if (!list) {
                  list = [];
                  itemsByType[item.type] = list;
              }
              list.push(item);
          }
          var weapons = this._checkNull(itemsByType['weapon']);

          var equippedWeapons = [];

          if(weapons.length > 0){
              for(let i = 0; i < weapons.length; i++){
                  if( weapons[i].data.location === "lefthand" || weapons[i].data.location === "righthand"){
                      equippedWeapons.push(weapons[i])
                  }
              }



          }else{

          }

      }else{

      }



      // Make separate methods for each Actor type (character, npc, etc.) to keep
      // things organized.
      if (actorData.type === 'character' || actorData.type === 'monster') this._prepareCharacterData(actorData);
  }

    _checkNull(items) {
        if (items && items.length) {
            return items;
        }
        return [];
    }

  /**
   * Prepare Character type specific data
   */
  _prepareCharacterData(actorData) {
    const data = actorData.data;

    //Lets calculate the players initiative modifier
    //data.init_mod = data.stats.wits;



  }


}
