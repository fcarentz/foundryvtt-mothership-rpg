<div align=center>

# Mothership RPG (unofficial)for Foundry VTT

<img title="Minimum core version" src="https://img.shields.io/badge/dynamic/json?url=https://gitlab.com/fcarentz/foundryvtt-mothership-rpg/-/raw/master/src/system.json&label=core&query=minimumCoreVersion&suffix=%2B&style=flat-square&color=important">
<img title="Latest compatible version" src="https://img.shields.io/badge/dynamic/json?url=https://gitlab.com/fcarentz/foundryvtt-mothership-rpg/-/raw/master/src/system.json&label=compatible&query=compatibleCoreVersion&style=flat-square&color=important">
<img src="https://img.shields.io/badge/dynamic/json?url=https://gitlab.com/fcarentz/foundryvtt-mothership-rpg/-/raw/master/src/system.json&label=version&query=version&style=flat-square&color=success">



</div>

An implementation of the [Mothership Role Playing](https://www.mothershiprpg.com) game system for [Foundry Virtual Tabletop](http://foundryvtt.com)
(<i>this is a fan-created product and has no affiliation with Tuesday Knight Games</i>)


## Installation Instructions

To install the Motehrship system for Foundry Virtual Tabletop, simply paste the following URL into the **Install System**
dialog on the Setup menu of the application.

https://gitlab.com/fcarentz/foundryvtt-mothership-rpg/-/raw/master/src/system.json

If you wish to manually install the system, extract it into the `Data/systems/mothership` folder.


## Local Build Instructions

To create a local build of the Mothership system for Foundry VTT, follow these steps:

1. If you don't Node.js installed then install the latest Node.js LTS version from here https://nodejs.org/en/
1. Clone the repository and either change into it or open a commandline/terminal window in the cloned the directory
1. Run the `npm install` command to install all the required node modules, including the type definitions.
1. Set the `dataPath` in `foundryconfig.json` to your FoundryVTT data folder.
1. Either run `npm run build` in a shell in your cloned directory or run the npm script `build` directly from your IDE, such as VS Code. Note: you might need to run this command as admin. To do this open your terminal or IDE as admin
1. Done, Mothership should now show up in Foundry VTT as an installed game system

> Note: I am not responsible for any local changes _you_ make to Mothership

## Community Contribution

Code and content contributions are accepted. Please feel free to submit issues to the issue tracker or submit merge
requests for code changes. Approval for such requests involves code and (if necessary) design review by FCarentz. Please
reach out on the Foundry Community Discord with any questions.

**Any merge requests submitted must be submitted with `develop` as the target branch. Merge requests that target the `master` branch will be rejected or ignored.**

## Example view/explanation

![Explainer Image](src/assets/Capture.JPG?raw=true)

